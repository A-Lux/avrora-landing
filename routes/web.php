<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = Request::segment(1);
$lang = true;

Route::group(['prefix' => url_routes() . '/admin/'], function () {
    Auth::routes();
    Route::get('', 'admin\MainController@index');
    Route::get('dev/{dev_status}', 'admin\MainController@developer');
    Route::get('table', 'admin\TablesController@index');
    Route::post('table', 'admin\GenerationController@model_position');

    Route::post('table_meta_update', 'admin\TablesController@table_meta_update');
    Route::get('table/{model_name}', 'admin\TablesController@edit');
    Route::get('column_name', 'admin\TablesController@column');
    Route::get('column_name/edit/{id}', 'admin\TablesController@column_edit');
    Route::post('update', 'admin\UpdateController@update_model');

    Route::group(['prefix' => 'orders/'], function () {
        Route::post('edit/{id}/updateStatus', 'admin\OrderController@status_set');
    });


    Route::post('model/{model_name}', 'admin\GenerationController@model_position');
    Route::get('model/{model_name}', 'admin\GenerationController@model_catalog');
    Route::get('model/{model_name}/{id}', 'admin\GenerationController@model_save');

    Route::get('StaticText', 'admin\MainController@s_text');
    Route::post('update_text', 'admin\MainController@update_text');
});


Route::group(['prefix' => url_routes() . '/'], function () {
    Auth::routes();
    Route::get('', 'client\MainController@redirect');
    Route::get('{type}', 'client\MainController@index');
    Route::post('/send_order', 'client\MainController@sendMessage')->name('main.index');
});
