-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 24 2020 г., 12:10
-- Версия сервера: 10.3.13-MariaDB
-- Версия PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `avrora-lan`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_catalogs`
--

CREATE TABLE `cicada_catalogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_cities`
--

CREATE TABLE `cicada_cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sites_name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coordinates` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sort` int(11) NOT NULL,
  `visable` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_cities`
--

INSERT INTO `cicada_cities` (`id`, `name`, `name_key`, `sites_name_key`, `coordinates`, `sort`, `visable`, `created_at`, `updated_at`) VALUES
(1, 'Алматы', 'almaty', 'mello_kz', '', 0, 1, '2020-08-20 06:45:23', '2020-08-20 06:45:23');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_column_names`
--

CREATE TABLE `cicada_column_names` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_failed_jobs`
--

CREATE TABLE `cicada_failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_filters`
--

CREATE TABLE `cicada_filters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_filter_attitudes`
--

CREATE TABLE `cicada_filter_attitudes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `FilterItem_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_filter_items`
--

CREATE TABLE `cicada_filter_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `Filter_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_languages`
--

CREATE TABLE `cicada_languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `visable` int(11) NOT NULL DEFAULT 0,
  `alternative` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_languages`
--

INSERT INTO `cicada_languages` (`id`, `name`, `name_key`, `visable`, `alternative`, `created_at`, `updated_at`) VALUES
(1, 'rus', 'ru', 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_migrations`
--

CREATE TABLE `cicada_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_migrations`
--

INSERT INTO `cicada_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_19_080231_create_permission_grups_table', 1),
(4, '2017_10_19_080327_create_permission_users_table', 1),
(5, '2018_02_06_074012_create_permission_dates_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2019_09_05_180425_create_languages_table', 1),
(8, '2019_09_05_203111_create_cities_table', 1),
(9, '2019_09_05_203252_create_orders_table', 1),
(10, '2019_09_05_203408_create_filters_table', 1),
(11, '2019_09_05_203421_create_filter_items_table', 1),
(12, '2019_09_05_203457_create_order_items_table', 1),
(13, '2019_09_05_204200_create_filter_attitudes_table', 1),
(14, '2019_09_05_204815_create_products_table', 1),
(15, '2019_09_05_204905_create_product_prices_table', 1),
(16, '2019_09_05_205437_create_catalogs_table', 1),
(17, '2019_09_05_214923_create_model_lists_table', 1),
(18, '2019_09_05_221710_create_model_metas_table', 1),
(19, '2019_09_05_222221_create_nav_menus_table', 1),
(20, '2019_09_05_224414_create_column_names_table', 1),
(21, '2019_12_20_050016_create_sites_table', 1),
(22, '2019_12_20_050043_create_shops_table', 1),
(23, '2019_12_26_131411_create_stocks_table', 1),
(24, '2019_12_26_131458_create_questions_table', 1),
(25, '2019_12_27_042414_create_news_items_table', 1),
(26, '2020_07_30_062941_create_static_texts_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_model_lists`
--

CREATE TABLE `cicada_model_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_model_lists`
--

INSERT INTO `cicada_model_lists` (`id`, `name`, `name_key`, `icon`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'Каталог', 'Catalog', NULL, 0, NULL, NULL),
(2, 'Продукция', 'Product', '<span class=\"oi\" data-glyph=\"battery-full\"></span>', 0, NULL, '2020-08-20 06:46:40'),
(3, 'Цена продукции', 'Product_price', NULL, 0, NULL, NULL),
(4, 'Названия Фильтров', 'Filter', NULL, 0, NULL, NULL),
(5, 'Пункты Фильтров', 'Filter_item', NULL, 0, NULL, NULL),
(6, 'Связи Фильтров', 'Filter_attitudes', NULL, 0, NULL, NULL),
(7, 'Информация о заказе', 'Order', NULL, 0, NULL, NULL),
(8, 'Информация о заказе', 'Order_item', NULL, 0, NULL, NULL),
(9, 'Пользователи', 'User', NULL, 0, NULL, NULL),
(10, 'Сайты', 'Site', NULL, 0, NULL, NULL),
(11, 'Города', 'City', NULL, 0, NULL, NULL),
(12, 'Магазины', 'Shop', NULL, 0, NULL, NULL),
(13, 'Языки', 'Language', NULL, 0, NULL, NULL),
(14, 'Текста', 'StaticText', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_model_metas`
--

CREATE TABLE `cicada_model_metas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typeInput` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languages` int(11) NOT NULL DEFAULT 0,
  `attachment` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_model_metas`
--

INSERT INTO `cicada_model_metas` (`id`, `name`, `name_key`, `sort`, `type`, `typeInput`, `languages`, `attachment`, `created_at`, `updated_at`) VALUES
(1, NULL, 'id', '0', 'table_catalog', NULL, 0, 'Product', '2020-08-20 06:46:40', '2020-08-20 06:46:40'),
(2, NULL, 'title', '0', 'table_catalog', NULL, 0, 'Product', '2020-08-20 06:46:40', '2020-08-20 06:46:40'),
(3, NULL, 'title', '0', 'table_save', NULL, 0, 'Product', '2020-08-20 06:46:40', '2020-08-20 06:46:40'),
(4, NULL, 'price', '0', 'table_save', NULL, 0, 'Product', '2020-08-20 06:46:40', '2020-08-20 06:46:40'),
(5, NULL, '1', '0', 'table_catalog_availability', NULL, 0, 'Product', '2020-08-20 06:46:40', '2020-08-20 06:46:40');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_nav_menus`
--

CREATE TABLE `cicada_nav_menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_db` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_news_items`
--

CREATE TABLE `cicada_news_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_orders`
--

CREATE TABLE `cicada_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `User_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_old` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `payment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promocode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_order_items`
--

CREATE TABLE `cicada_order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_old` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `ProductsPrice_nomenclature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Product_nomenclature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orders_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_password_resets`
--

CREATE TABLE `cicada_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_permission_dates`
--

CREATE TABLE `cicada_permission_dates` (
  `id` int(10) UNSIGNED NOT NULL,
  `permision_group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `crud` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'select',
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'db',
  `assess` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_permission_dates`
--

INSERT INTO `cicada_permission_dates` (`id`, `permision_group`, `table`, `crud`, `type`, `assess`, `created_at`, `updated_at`) VALUES
(1, '1', 'all', 'select', 'db', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_permission_grups`
--

CREATE TABLE `cicada_permission_grups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_permission_grups`
--

INSERT INTO `cicada_permission_grups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL),
(2, 'manage', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_permission_users`
--

CREATE TABLE `cicada_permission_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `grup_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_permission_users`
--

INSERT INTO `cicada_permission_users` (`id`, `user_id`, `grup_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_products`
--

CREATE TABLE `cicada_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ed_massa` int(11) NOT NULL DEFAULT 0,
  `ed_massa2` int(11) NOT NULL DEFAULT 0,
  `ed_count` int(11) NOT NULL DEFAULT 0,
  `let` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 5000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_products`
--

INSERT INTO `cicada_products` (`id`, `images`, `title`, `content`, `ed_massa`, `ed_massa2`, `ed_count`, `let`, `tags`, `price`, `size`, `slug`, `path`, `created_at`, `updated_at`, `sort`) VALUES
(1, NULL, '<p><strong>AkmaSept Prime 730 мл</strong></p>', NULL, 0, 0, 0, 0, NULL, '1200', NULL, 'akmasept-prime-730-ml-1', 'akmasept-prime-730-ml-1', '2020-08-20 06:47:17', '2020-08-20 06:48:51', 5000);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_product_prices`
--

CREATE TABLE `cicada_product_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_old` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` int(11) NOT NULL,
  `Product_id` int(11) DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Product_nomenclature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomenclature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_questions`
--

CREATE TABLE `cicada_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalog_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_shops`
--

CREATE TABLE `cicada_shops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `graphic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coordinates` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cities_name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_sites`
--

CREATE TABLE `cicada_sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_sites`
--

INSERT INTO `cicada_sites` (`id`, `name`, `name_key`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'mello.thefactory.kz', 'mello_kz', '0', '2020-08-20 06:45:23', '2020-08-20 06:45:23');

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_static_texts`
--

CREATE TABLE `cicada_static_texts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type_input` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `languages` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_static_texts`
--

INSERT INTO `cicada_static_texts` (`id`, `name_key`, `content`, `page`, `created_at`, `updated_at`, `type_input`, `languages`) VALUES
(1, 'Заголовок сайта', 'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ \"‎АВРОРА ХОЛДИНГ\"‎!', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(2, 'Заголовок сайта 2 текст', 'ВЫ ЗАБОТИТЕСЬ О СВОЕМ ЗДОРОВЬЕ, ЗДОРОВЬЕ СВОИХ СОТРУДНИКОВ, КЛИЕНТОВ ИЛИ ПАРТНЁРОВ?! ТОГДА НАШЕ ПРЕДЛОЖЕНИЕ ОБЯЗАТЕЛЬНО ВАС ЗАИНТЕРЕСУЕТ, ПОТОМУ ЧТО МЫ ПРЕДЛАГАЕМ ПРОДУКТ, КОТОРЫЙ ОБЕСПЕЧИТ ВЫСОКИЙ УРОВЕНЬ ЗАЩИТЫ ЗДОРОВЬЯ!', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(3, '1 блок красный левый текст', 'АНТИСЕПТИЧЕСКИЕ И ДЕЗИНФИЦИРУЮЩИЕ СРЕДСТВА \"AVRORA HOLDING\"', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(4, '1 блок черный левый текст', 'это эффективный способ оградить себя от инфекционных заболеваний и высокий уровень личной гигиены кожи в любых ситуациях!', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(5, '1 блок список разедлитель ;', '<pre>обладают широким спектром антимикробного и противовирусного действия;</pre>\r\n<pre>гипоаллергенные, не вызывают сухость кожи;</pre>\r\n<pre>не оказывают токсического и раздражающего эффекта;</pre>\r\n<pre>обладают пролонгированным антимикробным действием;</pre>\r\n<pre>не требуют смывания;</pre>\r\n<pre>подходят для частого применения;</pre>', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:31:19', 'textarea', 1),
(6, '2 блок красный верхний текст', 'ЛОКТЕВОГО ДОЗАТОР', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(7, '2 блок красный нижний текст', '\"AVRORA PRO\"', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(8, '2 блок описание под красным текстом', 'Предназначен для эффективного и экономичного использования антисептических и дезинфицирующих средств для рук.', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(9, '2 блок черный заголовок', 'ПРЕИМУЩЕСТВА:', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(10, '2 блок список разедлитель ;', '<pre>крепится стационарно на стене;</pre>\r\n<pre>нет контакта грязных рук и корпуса дозатора;</pre>\r\n<pre>экономное использование;</pre>\r\n<pre>многоразовое использование;</pre>', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:31:19', 'textarea', 1),
(11, '2 блок описание под списком', '<p><strong>ОБЕСПЕЧИВАЕТ</strong>&nbsp;правильную технологию обработки рук персонала в медицинских, лечебно-профилактических учреждениях, организациях общественного питания, школьных учреждениях, учреждениях соцобеспечения (дома престарелых, инвалидов и др.), маникюрных салонах, промышленных предприятиях, офисах и гостиницах.</p>', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:33:13', 'textarea', 1),
(12, '3 блок заголовок', 'Ценовое предложение', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(13, '3 блок описание', '<p>Бесплатная доставка по Казахстану<br />при покупке на сумму более 5000 тг.</p>', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:32:52', 'textarea', 1),
(14, '4 блок заголовок', 'Хотите узнать больше о нас?', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(15, '4 блок описание', '<p>Все вопросы вы можете задать по телефону + 7 727 313 11 88<br />По почте: info@avh.kz</p>', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:32:52', 'textarea', 1),
(16, '4 блок кнопка ссылка', NULL, 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:28:05', 'type_input', 1),
(17, '4 блок кнопка текст', 'ПЕРЕЙТИ НА САЙТ', 'antiseptic.Главная', '2020-08-20 23:27:16', '2020-08-20 23:27:16', 'type_input', 1),
(18, 'Заголовок', 'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ «АВРОРА ХОЛДИНГ»!', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:22:23', 'type_input', 1),
(19, '1 Блок Заголовок', '<p><strong>ДЕЗ БАРЬЕР ДЛЯ</strong></p>\r\n<p>ВНУТРЕННИХ ПОМЕЩЕНИЙ</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:12:39', 'textarea', 1),
(20, '1 Блок текст под красным текстом', '<p>Предназначен для снижения риска распространения коронавируса</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-11 05:55:22', 'textarea', 1),
(21, '1 блок список разедлитель ;', '<p>ДЛЯ ИСПОЛЬЗОВАНИЯ В ДОМЕ, ОФИСЕ, КАБИНЕТЕ, КАФЕ, САЛОНЕ КРАСОТЫ И ТП.;</p>\r\n<p>УБИВАЕТ 99,999% БАКТЕРИЙ И ВИРУСОВ;</p>\r\n<p>НЕ СОДЕРЖИТ ХЛОР, СПИРТ, ФОСФАТЫ;</p>\r\n<p>АБСОЛЮТНО БЕЗОПАСЕН ДЛЯ ЛЮДЕЙ, ЖИВОТНЫХ И РАСТЕНИЙ;</p>\r\n<p>ОБЕЗЗАРАЖИВАЕТ ЗА НЕСКОЛЬКО СЕКУНД;</p>\r\n<div class=\"list_it\"><span class=\"text  text-s16\">ДЕЗИНФИЦИРУЕТ ВСЕ ТЕЛО ЧЕЛОВЕКА И ЕГО ОДЕЖДУ;</span></div>\r\n<div class=\"list_it\"><span class=\"text  text-s16\">МОЖНО ОБЕЗЗАРАЖИВАТЬ СУМКИ, ПАКЕТЫ И Т.П.;</span></div>\r\n<div class=\"list_it\">&nbsp;</div>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-11 05:55:42', 'textarea', 1),
(22, '1 Блок текст под списком', '<p>Данная система мобильная, компактная и проста в использовании. <br />Она актуальна как во время, так и после пандемии коронавируса <br />и будет постоянно на защите вас и ваших близких</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-11 05:51:47', 'textarea', 1),
(23, '1 Блок текст кнопки', 'СТОП ВИРУС!', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:08:18', 'type_input', 1),
(24, '2 Блок текст заголовок', '<p class=\"text text-sf text-s33\">КОНСТРУКЦИЯ И УСТРОЙСТВО</p>\r\n<p class=\"text text-sf text-s33\">&nbsp;</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-01 23:10:04', 'textarea', 1),
(25, '2 Блок текст описание', '<p>Система&nbsp;<strong>ДЕЗ БАРЬЕР</strong> представляет собой разборную арочную конструкцию, которая легко собирается, не требует дополнительного крепления, сверления и другого сложного монтажа. Легко переносится и адаптируется под любые места установки, может регулироваться по ширине и высоте в зависимости от требований помещения/прохода. <br />Не портит и не загромождает интерьер.</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-11 05:56:50', 'textarea', 1),
(26, '2 Блок Заголовок списка', 'Принцип работы', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:08:18', 'type_input', 1),
(27, '2 блок список разедлитель ;', '<p>В ОСНОВУ ДЕЙСТВИЯ ДЕЗ БАРЬЕРА ПОЛОЖЕН ПРИНЦИП ДЕЗИНФЕКЦИОННОЙ ОБРАБОТКИ МЕЛКОДИСПЕРСНЫМ АЭРОЗОЛЬНЫМ СПРЕЕМ;</p>\r\n<p>НА ВХОДЕ В РАМКУ УСТАНОВЛЕН СЕНСОРНЫЙ ДАТЧИК, СРАБАТЫВАЮЩИЙ НА ДВИЖЕНИЕ;</p>\r\n<p>АВТОМАТИЧЕСКИ ЗАПУСКАЕТСЯ РАСПРЫСКИВАНИЕ ДЕЗИНФИЦИРУЮЩЕГО СРЕДСТВА;</p>\r\n<p>ЧЕРЕЗ СПЕЦИАЛЬНЫЕ ФОРСУНКИ ДЕЗСРЕДСТВО РАВНОМЕРНО ПОКРЫВАЕТ ВСЕ ТРУДНОДОСТУПНЫЕ МЕСТА ОДЕЖДЫ И ПОКРОВЫ ТЕЛА ЧЕЛОВЕКА;</p>\r\n<p>РЕЖИМ ОБРАБОТКИ НАСТРАИВАЕМЫЙ, СТАНДАРТНОЕ ЗНАЧЕНИЕ 3-5 СЕКУНД НА ЧЕЛОВЕКА.</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-11 05:55:22', 'textarea', 1),
(28, '3 Блок Заголовок', 'ЦЕНОВОЕ ПРЕДЛОЖЕНИЕ', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:08:18', 'type_input', 1),
(29, '3 Блок предложение', '<p>Система ДЕЗ БАРЬЕР &ndash; 150 000 тг</p>\r\n<p>Дезинфицирующий раствор:</p>\r\n<p>1 л &ndash; 350 тг (запас до 50 срабатываний)</p>\r\n<p>5 л &ndash; 1500 тг (запас до 250 срабатываний)</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-07 21:50:28', 'textarea', 1),
(30, '4 Блок заголовок', '<p>Возможна покупка в кредит через</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-07 21:50:28', 'textarea', 1),
(31, '4 Блок первая мини картинка', '/public/media/client/images/kaspi1.png', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:34:16', 'images', 1),
(32, '4 Блок вторая мини картинка', '/public/media/client/images/kaspi2.png', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:34:01', 'images', 1),
(33, '4 Блок описание', '<p>Бесплатная доставка по Казахстану<br />при покупке на сумму более 5000 тг.</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:08:18', 'textarea', 1),
(34, '5 Блок заголовок', 'Хотите узнать больше о нас?', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:04:13', 'type_input', 1),
(35, '5 Блок контакты', '<p>Ответим на все вопросы</p>\r\n<p>Т.: + 7 727 313 11 88<br />E.: info@avh.kz</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-07 21:58:43', 'textarea', 1),
(36, 'Кнопка ссылка', 'https://avh.kz/', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-08 21:31:51', 'type_input', 1),
(37, 'Кнопка текст', 'ПЕРЕЙТИ НА САЙТ', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-08-20 22:04:13', 'type_input', 1),
(38, 'Подвал текст', '<p>&copy; Copyright 2005-2020. Производственный комплекс &laquo;Аврора&raquo;.</p>\r\n<p>Производство и оптовые поставки бытовой химии, косметики и средств гигиены.</p>', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-07 21:56:32', 'textarea', 1),
(39, 'Cсылка facebook', 'https://www.facebook.com/avroracosmetics/', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-16 00:33:18', 'type_input', 1),
(40, 'Cсылка Instagram', 'https://www.instagram.com/avrora_brands/?hl=ru', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-08 21:34:58', 'type_input', 1),
(41, 'Cсылка youtube', 'https://www.youtube.com/AvroraHolding', 'disbarrier.Главная', '2020-08-20 22:04:13', '2020-09-11 05:55:22', 'type_input', 1),
(42, 'Логотип', '/public/media/client/disbarrier/images/logo.png', 'disbarrier.Header', '2020-08-20 22:35:50', '2020-09-23 05:47:39', 'images', 1),
(43, 'Заголовок header', '<p><strong>ҚАЗАҚСТАНДА ЖАСАЛҒАН</strong></p>\r\n<p>СДЕЛАНО В КАЗАХСТАНЕ</p>', 'disbarrier.Header', '2020-08-20 22:36:13', '2020-09-13 21:17:54', 'textarea', 1),
(44, 'Youtube ссылка', 'https://youtu.be/EonK9YXwtJE', 'disbarrier.Youtube', '2020-08-21 05:18:00', '2020-09-16 00:29:38', 'type_input', 1),
(45, 'Youtube превью', '/public/media/Update/image_jpeg/2JDtI_file.jpg', 'disbarrier.Youtube', '2020-08-21 05:29:16', '2020-09-13 23:00:00', 'images', 1),
(46, 'Youtube ссылка2', 'https://youtu.be/APQTO_Pf8EU', 'disbarrier.Youtube', '2020-08-26 04:36:13', '2020-09-13 23:00:00', 'type_input', 1),
(47, 'Youtube превью2', '/public/media/Update/image_jpeg/MZItD_file.jpg', 'disbarrier.Youtube', '2020-09-01 23:04:23', '2020-09-13 23:00:00', 'images', 1),
(48, 'Кнопка обратная связь', 'Связаться с нами', 'disbarrier.Главная', '2020-09-08 03:57:41', '2020-09-08 21:32:30', 'type_input', 1),
(49, 'Логотип ссылка', 'https://avh.kz/', 'disbarrier.Header', '2020-09-08 03:57:41', '2020-09-08 21:31:02', '/', 1),
(50, 'Фон первый блок', '/public/media/Update/image_png/Dm4Vs_file.png', 'disbarrier.Фоны', '2020-09-09 06:23:46', '2020-09-11 05:09:50', 'images', 1),
(51, 'Фон второый блок', '/public/media/Update/image_jpeg/kYejF_file.jpg', 'disbarrier.Фоны', '2020-09-09 06:23:46', '2020-09-10 02:47:09', 'images', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_stocks`
--

CREATE TABLE `cicada_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pattern` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cicada_users`
--

CREATE TABLE `cicada_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_confirm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `cicada_users`
--

INSERT INTO `cicada_users` (`id`, `name`, `email`, `tel`, `username`, `email_verified_at`, `password`, `code`, `is_confirm`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Олжас', 'fovarit38@gmail.com', NULL, 'admin', NULL, '$2y$10$SW3Q9hi3K/ME/A0E3AFrw.O.zyKzKn6WEcHULwYZSgKiUNvP6UyqS', NULL, '0', NULL, '2020-08-20 06:45:23', '2020-08-20 06:45:23');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cicada_catalogs`
--
ALTER TABLE `cicada_catalogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cicada_catalogs__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Индексы таблицы `cicada_cities`
--
ALTER TABLE `cicada_cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_column_names`
--
ALTER TABLE `cicada_column_names`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cicada_column_names_name_key_unique` (`name_key`);

--
-- Индексы таблицы `cicada_failed_jobs`
--
ALTER TABLE `cicada_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_filters`
--
ALTER TABLE `cicada_filters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_filter_attitudes`
--
ALTER TABLE `cicada_filter_attitudes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_filter_items`
--
ALTER TABLE `cicada_filter_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_languages`
--
ALTER TABLE `cicada_languages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_migrations`
--
ALTER TABLE `cicada_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_model_lists`
--
ALTER TABLE `cicada_model_lists`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_model_metas`
--
ALTER TABLE `cicada_model_metas`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_nav_menus`
--
ALTER TABLE `cicada_nav_menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_news_items`
--
ALTER TABLE `cicada_news_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_orders`
--
ALTER TABLE `cicada_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_order_items`
--
ALTER TABLE `cicada_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_password_resets`
--
ALTER TABLE `cicada_password_resets`
  ADD KEY `cicada_password_resets_email_index` (`email`);

--
-- Индексы таблицы `cicada_permission_dates`
--
ALTER TABLE `cicada_permission_dates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_permission_grups`
--
ALTER TABLE `cicada_permission_grups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_permission_users`
--
ALTER TABLE `cicada_permission_users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_products`
--
ALTER TABLE `cicada_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_product_prices`
--
ALTER TABLE `cicada_product_prices`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_questions`
--
ALTER TABLE `cicada_questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_shops`
--
ALTER TABLE `cicada_shops`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_sites`
--
ALTER TABLE `cicada_sites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_static_texts`
--
ALTER TABLE `cicada_static_texts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_stocks`
--
ALTER TABLE `cicada_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cicada_users`
--
ALTER TABLE `cicada_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cicada_users_email_unique` (`email`),
  ADD UNIQUE KEY `cicada_users_tel_unique` (`tel`),
  ADD UNIQUE KEY `cicada_users_username_unique` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cicada_catalogs`
--
ALTER TABLE `cicada_catalogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_cities`
--
ALTER TABLE `cicada_cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_column_names`
--
ALTER TABLE `cicada_column_names`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_failed_jobs`
--
ALTER TABLE `cicada_failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_filters`
--
ALTER TABLE `cicada_filters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_filter_attitudes`
--
ALTER TABLE `cicada_filter_attitudes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_filter_items`
--
ALTER TABLE `cicada_filter_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_languages`
--
ALTER TABLE `cicada_languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_migrations`
--
ALTER TABLE `cicada_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT для таблицы `cicada_model_lists`
--
ALTER TABLE `cicada_model_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `cicada_model_metas`
--
ALTER TABLE `cicada_model_metas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `cicada_nav_menus`
--
ALTER TABLE `cicada_nav_menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_news_items`
--
ALTER TABLE `cicada_news_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_orders`
--
ALTER TABLE `cicada_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_order_items`
--
ALTER TABLE `cicada_order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_permission_dates`
--
ALTER TABLE `cicada_permission_dates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_permission_grups`
--
ALTER TABLE `cicada_permission_grups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `cicada_permission_users`
--
ALTER TABLE `cicada_permission_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_products`
--
ALTER TABLE `cicada_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_product_prices`
--
ALTER TABLE `cicada_product_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_questions`
--
ALTER TABLE `cicada_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_shops`
--
ALTER TABLE `cicada_shops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_sites`
--
ALTER TABLE `cicada_sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `cicada_static_texts`
--
ALTER TABLE `cicada_static_texts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `cicada_stocks`
--
ALTER TABLE `cicada_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `cicada_users`
--
ALTER TABLE `cicada_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
