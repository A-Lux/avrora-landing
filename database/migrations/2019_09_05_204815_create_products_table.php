<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('images')->nullable();
            $table->string('title')->nullable();
            $table->longText('content')->nullable();
            $table->integer('ed_massa')->default(0);
            $table->integer('ed_massa2')->default(0);
            $table->integer('ed_count')->default(0);
            $table->integer('let')->default(0);
            $table->string('tags')->nullable();
            $table->string('price')->nullable();
            $table->string('size')->nullable();
            $table->text('slug')->nullable();
            $table->text('path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
