<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionGrupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_grups', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
            $table->timestamps();
        });

        DB::table('permission_grups')->insert(
            array(
                'name' => 'Admin',
            )
        );

        DB::table('permission_grups')->insert(
            array(
                'name' => 'manage',
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_grups');
    }
}
