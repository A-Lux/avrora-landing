<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('permision_group')->default('');
            $table->string('table');
            $table->string('crud')->default('select');
            $table->string('type')->default('db'); // sing
            $table->integer('assess')->default(0);
            $table->timestamps();
        });

        DB::table('permission_dates')->insert([
            [
                'table' => 'all',
                'permision_group' => '1',
                'crud' => 'select',
                'assess' => 1,
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_dates');
    }
}
