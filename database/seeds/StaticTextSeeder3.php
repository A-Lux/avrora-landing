<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StaticTextSeeder3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('static_texts')->insert(
            [
             'name_key' =>'5 Блок заголовок antiseptic',
             'content' =>'Хотите узнать больше о нас?',
             'page' =>'antiseptic.Главная',
             'type_input' =>'type_input',
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' =>'5 Блок контакты antiseptic',
                'content' =>'<p>Ответим на все вопросы</p>
<p>Т.: + 7 727 313 11 88<br />E.: info@avh.kz</p>',
                'page' =>'antiseptic.Главная',
                'type_input' =>'textarea',
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' =>'Email Antiseptic',
                'content' =>'1@avh.kz',
                'page' =>'emails',
                'type_input' =>'type_input',
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' =>'Email Disbarrier',
                'content' =>'1@avh.kz',
                'page' =>'emails',
                'type_input' =>'type_input',
            ]
        );

//        $v = \App\StaticText::where('name_key', 'Кнопка обратная связь antiseptic')
//            ->where('page', 'antiseptic.Главная')->first();
//
//        $v->delete();
//
//        $v = \App\StaticText::where('name_key', 'Заголовок сайта')
//            ->where('content', 'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ "‎АВРОРА ХОЛДИНГ"‎!')
//            ->where('page', 'antiseptic.Главная')->get();
//        foreach ($v as $item) {
//            $item->delete();
//        }
//
//        $v = \App\StaticText::where('name_key', 'Заголовок сайта 2 текст')
//            ->where('content', 'ВЫ ЗАБОТИТЕСЬ О СВОЕМ ЗДОРОВЬЕ, ЗДОРОВЬЕ СВОИХ СОТРУДНИКОВ, КЛИЕНТОВ ИЛИ ПАРТНЁРОВ?! ТОГДА НАШЕ ПРЕДЛОЖЕНИЕ ОБЯЗАТЕЛЬНО ВАС ЗАИНТЕРЕСУЕТ, ПОТОМУ ЧТО МЫ ПРЕДЛАГАЕМ ПРОДУКТ, КОТОРЫЙ ОБЕСПЕЧИТ ВЫСОКИЙ УРОВЕНЬ ЗАЩИТЫ ЗДОРОВЬЯ!')
//            ->where('page', 'antiseptic.Главная')->get();
//
//        foreach ($v as $item) {
//            $item->delete();
//        }
//
//        $v = new \App\StaticText();
//        $v->id = 4;
//        $v->name_key = 'Заголовок сайта';
//        $v->content = 'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ "‎АВРОРА ХОЛДИНГ"‎!';
//        $v->page = 'antiseptic.Главная';
//        $v->type_input = 'type_input';
//        $v->languages = 1;
//        $v->save();
//
//        $v = new \App\StaticText();
//        $v->id = 5;
//        $v->name_key = 'Заголовок сайта 2 текст';
//        $v->content = 'ВЫ ЗАБОТИТЕСЬ О СВОЕМ ЗДОРОВЬЕ, ЗДОРОВЬЕ СВОИХ СОТРУДНИКОВ, КЛИЕНТОВ ИЛИ ПАРТНЁРОВ?! ТОГДА НАШЕ ПРЕДЛОЖЕНИЕ ОБЯЗАТЕЛЬНО ВАС ЗАИНТЕРЕСУЕТ, ПОТОМУ ЧТО МЫ ПРЕДЛАГАЕМ ПРОДУКТ, КОТОРЫЙ ОБЕСПЕЧИТ ВЫСОКИЙ УРОВЕНЬ ЗАЩИТЫ ЗДОРОВЬЯ!';
//        $v->page = 'antiseptic.Главная';
//        $v->type_input = 'type_input';
//        $v->languages = 1;
//        $v->save();
    }
}
