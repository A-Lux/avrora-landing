<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StaticTextSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 1; $i <= 11; $i++) {
            $v = \App\StaticText::find($i);
            $v->delete();
        }

        \App\StaticText::find(13)->delete();


        DB::table('static_texts')->insert(
            [
                'name_key' => 'Способы оплаты изображение 1 antiseptic',
                'content' => '/public/media/client/disbarrier/images/kaspi1.png',
                'page' => 'antiseptic.Главная',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Способы оплаты изображение 2 antiseptic',
                'content' => '/public/media/client/disbarrier/images/kaspi2.png',
                'page' => 'antiseptic.Главная',
                'type_input' => 'images'
            ]
        );
    }
}
