<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StaticTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('static_texts')->insert(
            [
                'name_key' => 'Cсылка facebook antiseptic',
                'content' => '',
                'page' => 'antiseptic.Главная',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Cсылка Instagram antiseptic',
                'content' => '',
                'page' => 'antiseptic.Главная',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Cсылка youtube antiseptic',
                'content' => '',
                'page' => 'antiseptic.Главная',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Логотип antiseptic',
                'content' => '',
                'page' => 'antiseptic.Header',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Подвал текст antiseptic',
                'content' => '',
                'page' => 'antiseptic.Главная',
                'type_input' => 'textarea'
            ]
        );



        DB::table('static_texts')->insert(
            [
                'name_key' => 'Youtube ссылка antiseptic',
                'content' => '',
                'page' => 'antiseptic.Youtube',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Youtube превью antiseptic',
                'content' => '',
                'page' => 'antiseptic.Youtube',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Youtube ссылка2 antiseptic',
                'content' => '',
                'page' => 'antiseptic.Youtube',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Youtube превью2 antiseptic',
                'content' => '',
                'page' => 'antiseptic.Youtube',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Кнопка обратная связь antiseptic',
                'content' => 'Связаться с нами',
                'page' => 'antiseptic.Главная',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Логотип ссылка antiseptic',
                'content' => '/public/media/client/antiseptic/images/logo.png',
                'page' => 'antiseptic.Header',
                'type_input' => '/'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Фон первый блок antiseptic',
                'content' => 'https://avh.kz/',
                'page' => 'antiseptic.Фоны',
                'type_input' => 'images'
            ]
        );
        DB::table('static_texts')->insert(
            [
                'name_key' => 'Фон второй блок antiseptic',
                'content' => 'https://avh.kz/',
                'page' => 'antiseptic.Фоны',
                'type_input' => 'images'
            ]
        );


        DB::table('static_texts')->insert(
            [
                'name_key' => 'Фон первый блок планшет antiseptic',
                'content' => 'https://avh.kz/',
                'page' => 'antiseptic.Фоны',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Фон второй блок планшет antiseptic',
                'content' => 'https://avh.kz/',
                'page' => 'antiseptic.Фоны',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Фон первый блок мобилка antiseptic',
                'content' => 'https://avh.kz/',
                'page' => 'antiseptic.Фоны',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Фон второй блок мобилка antiseptic',
                'content' => 'https://avh.kz/',
                'page' => 'antiseptic.Фоны',
                'type_input' => 'images'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'title antiseptic',
                'content' => 'Антисептик',
                'page' => 'antiseptic.Главная',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'title disbarrier',
                'content' => 'Диз',
                'page' => 'disbarrier.Главная',
                'type_input' => 'type_input'
            ]
        );

        DB::table('static_texts')->insert(
            [
                'name_key' => 'Кнопка обратная связь antiseptic',
                'content' => 'Обратная свзяь',
                'page' => 'antiseptic.Главная',
                'type_input' => 'type_input'
            ]
        );

    }
}
