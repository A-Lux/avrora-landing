<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_meta extends Model
{
    public function model_name()
    {
        return $this->belongsTo('App\Model_list', 'attachment', "name_key")->first();
    }
}
