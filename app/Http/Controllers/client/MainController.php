<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

use App\Mail\FeedbackMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use Validator;

class MainController extends Controller
{

    public function __construct()
    {


    }

    public function redirect()
    {
//        $text = preg_replace('|([/]+)|s', '/', url_custom('/antiseptic'));
        $url_list = [s_("Alias сайт 1", "alias url", 'antiseptic'), s_("Alias сайт 2", "alias url", 'disbarrier')];
//        return redirect()->to("/" . str_replace($url_list, ['antiseptic', 'disbarrier'], $url_list[0]));
        return redirect()->to("/" . $url_list[0]);

    }

    public function index()
    {
        $url_path = explode("/", $_SERVER["REQUEST_URI"]);
        $url_list = [s_("Alias сайт 1", "alias url", 'antiseptic'), s_("Alias сайт 2", "alias url", 'disbarrier')];
        $elut=['antiseptic', 'disbarrier'];
        $key_index = array_search($url_path[1], $url_list);

        return view(''.$elut[$key_index].'.views.main');
    }


    public function sendMessage(Request $request)
    {
        $data = $request->all();
//        $data['phone'] = str_replace('-', '', $data['phone']);
//        $data['phone'] = str_replace('(', '', $data['phone']);
//        $data['phone'] = str_replace(')', '', $data['phone']);
//        $data['phone'] = str_replace(' ', '', $data['phone']);
//        $data['phone'] = trim($data['phone']);
        $validator = Validator::make($data,
            [
                'name' => 'required|string|max:255',
                'tel' => 'required|string|min:11|max:11',
                'email' => 'required|email|max:255',
                'message' => 'required|string',
                'from' => 'nullable',
            ],
            [
                'email.required' => 'Введите валидный email адрес',
                'email.email' => 'Введите валидный email адрес',
                'name.required' => 'Поле "Имя" обязательно для заполнения',
                'tel.required' => 'Поле "Телефон" обязательно для заполнения',
                'tel.min' => 'Поле "Телефон" должно содержать 11 символов',
                'tel.max' => 'Поле "Телефон" должно содержать 11 символов',
                'message.required' => 'Поле "Сообщение" обязательно для заполнения',
            ]);


        if ($validator->passes()) {
            if ($data['from'] == 'Antiseptic') {
                $toEmail = s_("Email Antiseptic", "emails", "", "type_input");
            } else {
                $toEmail = s_("Email Disbarrier", "emails", "", "type_input");
            }
//            Mail::to($toEmail)->send(new FeedbackMail($data));
            $html = "Имя:" . $data['name'] .
                "<br>" .
                "Email:" . $data['email'] .
                "<br>" .
                "Телефон:" . $data['tel'] .
                "<br>" .
                "Сообщение:" . $data['message'];
            Mail::send([], [], function (Message $message) use ($html, $toEmail, $data) {
                $message->to($toEmail)
                    ->subject($data['from'])
                    ->from('promo.avrora@bk.ru')
                    ->setBody($html, 'text/html');
            });

            return response()->json(['success' => 'ok']);
        }
        return response()->json(['error' => $validator->errors()->all()]);
    }


}
