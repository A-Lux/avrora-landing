@extends('auth.layouts.app')

@section('content')

    <div class="bulpForm">
        <div class="logo">
            <a href="/">{{ config('app.name') }}</a>
        </div>
        <div class="bulpForm_body">
            <div class="bulpForm_body_head">
                <h2 class="msg">Регистрация нового пользователя</h2>
            </div>
            <form id="sign_in" role="form" class="bulpForm_body_form" method="POST" autocomplete="off"
                  action="{{ url_custom('/register') }}">
                {{ csrf_field() }}


                {{maskInput($errors,["name"=>"name","type"=>"text","placeholder"=>"Имя","auto"=>false])}}
                {{maskInput($errors,["name"=>"email","type"=>"email","placeholder"=>"Email","auto"=>false])}}
                {{maskInput($errors,["name"=>"password","type"=>"password","placeholder"=>"Пароль","auto"=>false])}}
                {{maskInput($errors,["name"=>"password_confirmation","type"=>"password","placeholder"=>"Повторите пароль","auto"=>false])}}


                <div class="autocheck">
                    <a href="{{url('/admin/login')}}">У меня есть аккаунт</a>
                    <button class="btn  btn-success flat-buttons   waves-effect" type="submit">Регистрация</button>
                </div>

            </form>
        </div>
    </div>

@stop
