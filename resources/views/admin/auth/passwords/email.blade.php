@extends('auth.layouts.app')

@section('content')

<div class="bulpForm">
    <div class="logo">
        <a href="/">{{ config('app.name') }}</a>
    </div>
    <div class="bulpForm_body">
        <div class="bulpForm_body_head">
            <h2 class="msg">Сброс пароля</h2>
        </div>
        <form id="sign_in" class="bulpForm_body_form" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}


            {{maskInput($errors,["name"=>"email","type"=>"email","placeholder"=>"Email"])}}

            {{ $errors->has('password') ? ' error' : '' }}
            <div class="autocheck">
                <button type="submit" style="width: 100%;" class="btn  btn-success flat-buttons   waves-effect  ">Отправить ссылку для сброса пароля</button>
            </div>
            <div class="navlink">
                <a href="/login">У меня есть аккаунт</a>
                <a href="{{ url('/register') }}">Регистрация</a>
            </div>
        </form>
    </div>
</div>

<style>
    body {
        background-color: #00BCD4;
    }
</style>



@endsection
