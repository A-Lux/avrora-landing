@extends('views.layouts.app')

@section('content')


    <div class="header">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-4 border-bottom">
            <h1 class="h2">{{GMN($model_name)}}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group">
                    <a href="{{url_custom("/admin/model/".$model_name."/0")}}"
                       class="btn btn-sm btn-outline-secondary waves-effect  ">Добавление записи</a>
                </div>
            </div>
        </div>
    </div>

    <div class="bodyMain">
        @if(View::exists('views.generation.input.'.$model_name.'._table'))
            @include('views.generation.input.'.$model_name.'._table')
            <?php
            include('../resources/views/admin/views/generation/input/' . $model_name . '/_table.php')
            ?>
        @endif

        @include('constituent_element.table.main')

    </div>



@endsection
