@extends('client.layouts.app')

@section('content')

<div class="container catalog" style="text-align: center; ">

    <h1 class="titleh2 text text-s30" style="margin-top: 2rem;   width: 100%;">
        Забыл пароль?
    </h1>
    <div class="container_product">
        <div class="loginCOntrol" style="justify-content: center;">


            <div class="bulpForm " style="margin-bottom: 6rem; width: 360px;">
                <img src="/public/media/client/img/kost.png" alt="" style="margin-bottom: 1rem;">

                <div class="bulpForm_body ">
                    <div class="bulpForm_body_head">
                        <h2 class="msg text text-s18"></h2>
                    </div>


                    <b class="text text-s15 bulpForm_body_after" style="font-weight: bold; color:#000;">


                        Для восстановления пароля, <br>
                        укажите ваш E-mail


                        </br>


                        <form class="bulpForm_body_form" role="form" method="post" id="resetbox"
                              action="/resetPass">
                            {{ csrf_field() }}




                            <?php
                            if (\Session::has('code')) {
                                ?>

                                <div class="formnew" style="margin-bottom: 1rem;">
                                    <input type="text"
                                           name="code" class="form-control text text-s13" id="2Kr7qg" value=""
                                           style="text-align: center; width: 100%; padding: 0.25rem; box-sizing: border-box; margin: 0.5rem 0;" placeholder="Код" im-insert="true">
                                    <input type="text"
                                           name="password" class="form-control text text-s13" id="2sdKr7qg" value=""
                                           style="text-align: center; width: 100%; padding: 0.25rem; box-sizing: border-box; margin: 0.5rem 0;" placeholder="Новый пароль" im-insert="true">

                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="form-group form-link   " style=" margin-bottom: 1rem; margin-top: 1rem; ">


                                    <input type="tel" pattern="\+7\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}"
                                           name="tel" class="form-control text text-s13" id="2Kr7qg" value=""
                                           style="text-align: center;" placeholder="Телефон" im-insert="true">

                                </div>
                                <?
                            }
                            ?>



                            <p class="text text-s13">
                                На указанный электронный адрес вам будет выслано
                                письмо с инструкцией для смены пароля
                            </p>
                            <div class="autocheck">

                                <div class="form-check">

                                </div>


                                <div class="form-group form-link"
                                     style="display: flex;align-items: center;justify-content: center;">
                                    <button type="submit" class="btn  btn-success text text-s13 "
                                            style=" flex-grow: 0; width: auto; padding: 0.5rem 1rem; ">ПОЛУЧИТЬ ПАРОЛЬ
                                    </button>


                                </div>


                            </div>
                            <!--                <div class="navlink">-->
                            <!--                    <a href="{{ url('/register') }}">Регистрация</a>-->
                            <!--                    <a href="{{ url('/password/reset') }}">Забыли пароль?</a>-->
                            <!--                </div>-->
                        </form>
                </div>
            </div>


        </div>


    </div>
</div>

@endsection
