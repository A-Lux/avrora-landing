@extends('disbarrier.views.layouts.app')

@section('content')

    <div class="head">
        <h1 class="text text-s55 head_h1">
            {!! s_("Заголовок","disbarrier.Главная","","") !!}
        </h1>
    </div>


    <div class="section new-block new-block-one" style="position: relative">


        <div class="container text-container text-container-man text-container-tho">
            <div class="bkimg-def"
                 style="background-image: url('{!! s_("Фон первый блок","disbarrier.Фоны","/public/media/client/images/man.png","images") !!}');"></div>
            @if(str_replace("","",strip_tags(s_("Youtube ссылка","disbarrier.Youtube","",""))) !="")
                <a target="_blank" href="{!! s_("Youtube ссылка","disbarrier.Youtube","","") !!}" class="video_yy"
                   style="background-image: url('{!! s_("Youtube превью","disbarrier.Youtube","/public/media/client/images/open_video.png","") !!}');">

                </a>
            @endif
            <div class="text-item">
                <div class="text-item_head text-item_head-thos">
                    <div class="text text-s33">
                        {!! s_("1 Блок Заголовок","disbarrier.Главная","","") !!}

                    </div>
                </div>
                <div class="text-item_main text-item_main-two">
                    <div class="text text-sf text-s19">
                        {!! s_("1 Блок текст под красным текстом","disbarrier.Главная","","textarea") !!}
                    </div>
                </div>
            </div>

            <div class="list">

                @php
                    $desk=   explode(";",strip_tags(s_("1 блок список разедлитель ;","disbarrier.Главная",'',"textarea")));

                @endphp
                @foreach($desk as $ds)
                    @if(str_replace([" ","&nbsp"],"",$ds)!="")
                        <div class="list_it">
                            <span class="text text-s16">{{$ds}}</span>
                        </div>
                    @endif
                @endforeach


            </div>

            <div class="end_text">
                <div class="text text-sf text-s17" style="font-weight: 700;color: #2B2C30;">
                    {!! s_("1 Блок текст под списком","disbarrier.Главная","","textarea") !!}
                </div>
            </div>

            <div class="btn btn-stop"><span
                    class="text text-sf text-s34">{!! s_("1 Блок текст кнопки","disbarrier.Главная","","") !!} </span>
            </div>

        </div>
    </div>

    <div class="section new-block  new-block-tws" style="position: relative;">
        <div class="container text-container text-container-scrimer ">
            <div class="bkimg-def"
                 style="background-image: url('{!! s_("Фон второый блок","disbarrier.Фоны","/public/media/client/images/scrimer.png","images") !!}');"></div>
            @if(str_replace("","",strip_tags(s_("Youtube ссылка2","disbarrier.Youtube","",""))) !="")
                <a target="_blank" href="{!! s_("Youtube ссылка2","disbarrier.Youtube","","") !!}" class="video_yy"
                   style="background-image: url('{!! s_("Youtube превью2","disbarrier.Youtube","/public/media/client/images/open_video.png","") !!}');z-index: 10000">
                </a>
            @endif
            <div class="text-item">
                <div class="text-item_head text-item_head-thos">
                    <p class="text text-sf text-s33">
                        {!! s_("2 Блок текст заголовок","disbarrier.Главная","","textarea") !!}
                    </p>
                </div>
                <div class="text-item_main text-item_main-two" style=" font-weight: 400; ">
                    <div class="text text-sf text-s17">
                        {!! s_("2 Блок текст описание","disbarrier.Главная","","textarea") !!}
                    </div>
                </div>
            </div>
            <h2 style=" width: 100%; ">
                            <span
                                class="text text-sf text-s19"><b> {!! s_("2 Блок Заголовок списка","disbarrier.Главная","","") !!}</b></span>
            </h2>
            <div class="list">

                @php
                    $desk=   explode(";",strip_tags(s_("2 блок список разедлитель ;","disbarrier.Главная",'',"textarea")));
                @endphp
                @foreach($desk as $ds)
                    @if(str_replace([" ","&nbsp"],"",$ds)!="")
                        <div class="list_it">
                            <span class="text text-s16">{{$ds}}</span>
                        </div>
                    @endif
                @endforeach

            </div>


        </div>
    </div>


    <div class="container container-mini section table-block" style="position: relative;">
        <div class="main_table">
            <h2 class="hed-text">
                <b class="text text-sf text-s33"
                   style="color: #ED3237;text-transform: uppercase;">{!! s_("3 Блок Заголовок","disbarrier.Главная","","") !!}</b>
            </h2>

            <div class="text text-sf text-price text-s33" style="font-weight: 600; color: #2B2C30;">
                {!! s_("3 Блок предложение","disbarrier.Главная","","textarea") !!}
            </div>

        </div>

        <div class="footer-text">
            <div class="footer-text_main">
                <div class="text text-sf text-s17">
                    {!! s_("4 Блок заголовок","disbarrier.Главная","","textarea") !!}
                </div>
                <div class="icon-cont">
                    <div class="img-icon">
                        <div class="prop">
                            <div class="prop_img prop_img-65">
                                <div class="prop_img_src"
                                     style="background-image: url('{!! s_("4 Блок первая мини картинка","disbarrier.Главная","/public/media/client/disbarrier/images/kaspi1.png","images") !!}');">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="img-icon">
                        <div class="prop">
                            <div class="prop_img prop_img-65">
                                <div class="prop_img_src"
                                     style="background-image: url('{!! s_("4 Блок вторая мини картинка","Главная","/public/media/client/disbarrier/images/kaspi2.png","images") !!}');">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-text-end">
                    <div class="text text-sf text-s19">
                        {!! s_("4 Блок описание","disbarrier.Главная","","textarea") !!}
                    </div>
                </div>
            </div>
        </div>


    </div>


    {{--    <section>--}}
    {{--        <div class="container">--}}
    {{--          --}}
    {{--        </div>--}}
    {{--    </section>--}}

    <section class="footesad">
        <div class="container container-mini section section-end">
            <div class="footer">
                <div class="footer_main">
                    <div class="text text-sf text-s20 folinsa">
                        <b>{!! s_("5 Блок заголовок","disbarrier.Главная","Хотите узнать больше о нас?","") !!} </b>
                        <br>
                        {!! s_("5 Блок контакты","disbarrier.Главная","","textarea") !!}
                    </div>

                    <a href="javascript:void(0)" class="btn btn-link btn-link-new open-model">
                        <span class="text text-cal text-s19"
                              style="color:#fff;text-transform: uppercase;">{!! s_("Кнопка обратная связь","disbarrier.Главная","обратная связь") !!}</span>
                    </a>
                    <a href="{!! s_("Кнопка ссылка","disbarrier.Главная","","") !!}" target="_blank"  class="btn btn-link">
                    <span
                        class="text text-cal text-s19">{!! s_("Кнопка текст","disbarrier.Главная","ПЕРЕЙТИ НА САЙТ","") !!}</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="footer_after container">
            <div class="text textblas text-s10">
                {!! s_("Подвал текст","disbarrier.Главная","ПЕРЕЙТИ НА САЙТ","textarea") !!}
            </div>
            <div class="social">

                <a href="{!! s_("Cсылка facebook","disbarrier.Главная","ПЕРЕЙТИ НА САЙТ") !!}" class="soc">
                    <img src="/public/media/client/disbarrier/images/faceboo.png" alt="">
                </a>

                <a href="{!! s_("Cсылка Instagram","disbarrier.Главная","ПЕРЕЙТИ НА САЙТ") !!}" class="soc">
                    <img src="/public/media/client/disbarrier/images/instag.png" alt="">
                </a>

                <a href="{!! s_("Cсылка youtube","disbarrier.Главная","ПЕРЕЙТИ НА САЙТ") !!}" class="soc">
                    <img src="/public/media/client/disbarrier/images/yout.png" alt="">
                </a>

            </div>
        </div>
    </section>
@endsection
