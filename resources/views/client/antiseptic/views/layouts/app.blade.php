<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{!! s_("title antiseptic","antiseptic.Главная",'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ "‎АВРОРА ХОЛДИНГ"‎!') !!} </title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/public/media/client/antiseptic/fonts/Calibri/stylesheet.css">
    <link rel="stylesheet" href="/public/media/client/antiseptic/css/style.css?v=0.3">
    {{--    <link rel="stylesheet" href="/public/media/client/disbarrier/css/style.css?v=0.3">--}}


    <meta name="description" content="">
    <meta name="Keywords" content="">
    <!-- Bootstrap CSS -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <!-- Global site tag (gtag.js) - Google Ads: 494836667 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-494836667"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-494836667');
    </script>


</head>
<body>
<!-- Event snippet for Website sale conversion page -->
<script>
    gtag('event', 'conversion', {
        'send_to': 'AW-494836667/a0GeCLWlueUBELu3-usB',
        'transaction_id': ''
    });
</script>


<style>

    .form_main {
        padding: 0;
        padding: 1.5rem;
        padding-top: 1.5rem;
        width: 400px;
        max-width: 100%;
    }

    .input_in {
        background-color: #e8e8e8;
        padding: 0.5rem;
        border: 1px solid #ccc;
        margin-top: 0.5rem;
        width: 100%;
    }

    .input + .input {
        margin-top: 1.5rem;
    }
</style>


<main>


    <header class="header">
        <div class="header_main container">
            <div class="header_main_pos">
                <div class="logo-text text text-sf text-s18">
                    {!! s_("Заголовок header antiseptic","Header","","textarea") !!}
                </div>
                <a href="{{s_("Логотип Ссылка Antiseptic","antiseptic.Header","","/")}}" target="_blank" class="logo">
                    <img src="{{s_("Логотип antiseptic","antiseptic.Header","","/")}}"
                         alt="">
                </a>
            </div>
        </div>
    </header>
    {{--    <header class="header">--}}
    {{--        <div class="header_main container">--}}
    {{--            <a href="/" class="logo">--}}
    {{--                <img src="/public/media/client/antiseptic/images/logo.png" alt="">--}}
    {{--            </a>--}}
    {{--        </div>--}}
    {{--    </header>--}}


    <div class="form" style="display: none">

        <form id="form" action="{{url_custom('/send_order')}}"
              {{--          onsubmit="event.preventDefault(); formSubmit();"--}}
              {{--          onSubmit="formSubmit();return false;" --}}
              class="form_main sen_forss"
              style="text-align: center;">
            <h2 style=" font-size: 1.25rem; margin-bottom: 1.5rem; color: #ed3237; text-transform: uppercase; ">
                обратная связь
            </h2>

            <div class="input">
                <div class="input_lab text text-s16">Имя</div>
                <input class="input_in text text-s16 name-input" required name="name" type="text">
            </div>
            <div class="input">
                <div class="input_lab text text-s16">Телефон</div>
                <input class="input_in text text-s16" required name="tel" type="number">
            </div>

            <div class="input">
                <div class="input_lab text text-s16">E-mail</div>
                <input class="input_in text text-s16" name="email" type="text">
            </div>

            <div class="input">
                <div class="input_lab text text-s16">Сообщение</div>
                <textarea style="max-width: 100%;min-width: 100%;max-height: 200px;" class="input_in text text-s16"
                          name="message" type="text"></textarea>
            </div>
            <button class="btn btn-link" id="send__btnv"
                    style="position: relative; right: 0; margin: auto; margin-top: 2.5rem; ">
                <span class="text text-cal text-s19" style="color:#fff;text-transform: uppercase;">отправить</span>
            </button>

        </form>

    </div>

    @yield('content')


</main>


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/public/media/client/antiseptic/js/app.js?v=0.2"></script>
<script src="/public/media/client/disbarrier/js/app.js?v=0.46"></script>


<script>

    function openSuccessModal() {
        $.fancybox.open('<div class="message" style="font-size: 1.5rem;padding: 30px">' +
            '<img src="/public/media/client/disbarrier/images/pngegg.png" style="display:flex;width:90px;margin:0 auto;margin-bottom: 10px">' +
            '<p>Ваше сообщение было отправлено</p>' +
            '<p>Мы свяжемся с Вами как можно скорее</p>' +
            '    <button class="btn btn-link" id="close_success" data-fancybox-close' +
            '            style="position:relative; right: 0; margin: auto; margin-top: 2.5rem; ">' +
            '        <span class="text text-cal text-s19" style="color:#fff;text-transform: uppercase;">Продолжить</span>' +
            '    </button>' +
            '</div>');
    }

    function openErrorModal(msg) {
        var err = '';
        for (var i of msg) {
            err += '<li style="margin-bottom: 3px;color: red">' + i + '</li>';
        }
        $.fancybox.open('<div class="message" style="font-size: 1.5rem;padding: 30px">' +
            '<div id="qwd">' +
            '<p>Исправьте ошибки:</p>' +
            '<ul>' +
            err +
            '</ul>' +
            '</div>' +
            '    <button class="btn btn-link" id="close_success" data-fancybox-close' +
            '            style="position:relative;right: 0; margin: auto; margin-top: 2.5rem; ">' +
            '        <span class="text text-cal text-s19" style="color:#fff;text-transform: uppercase;">Продолжить</span>' +
            '    </button>' +
            '</div>'
        )
        ;
    }


    function getFormData($form) {
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};


        $.map(unindexed_array, function (n, i) {


            if (typeof indexed_array[n['name']] != "undefined") {


                if (Array.isArray(indexed_array[n['name']])) {
                    indexed_array[n['name']].push(n['value'])
                } else {
                    indexed_array[n['name']] = [n['value'], indexed_array[n['name']]];
                }

            } else {
                indexed_array[n['name']] = n['value'];
            }


        });

        return indexed_array;
    }

    $(document).on("submit", ".sen_forss", function () {
        var $this = $(this);
        var inputs = getFormData($this);
        let name = inputs.name;
        let tel = inputs.tel;
        let email = inputs.email;
        let message = inputs.message;
        let from = 'Antiseptic';
        $.ajax({
            url: "/send_order",
            type: 'POST',
            data: {from, name, email, tel, message, "_token": "{{ csrf_token() }}"},
            success: function (data) {
                if ($.isEmptyObject(data.error)) {
                    $.fancybox.close();
                    $('#errors').addClass('d-none');
                    openSuccessModal();
                    $('form').trigger('reset');
                } else {
                    openErrorModal(data.error);
                }
            }
        });
        return false;
    });


</script>


</body>
</html>
