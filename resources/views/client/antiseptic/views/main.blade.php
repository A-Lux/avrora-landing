@extends('antiseptic.views.layouts.app')

@section('content')


    <div class="container">
        <div class="head">
            <h1 class="text text-s32 head_h1">
                <b>{!! s_("Заголовок сайта","antiseptic.Главная",'ГОРЯЧЕЕ ПРЕДЛОЖЕНИЕ ОТ КОМПАНИИ "‎АВРОРА ХОЛДИНГ"‎!') !!} </b>
            </h1>
            <p class="text text-s18 head_after">
                {!! s_("Заголовок сайта 2 текст","antiseptic.Главная",'ВЫ ЗАБОТИТЕСЬ О СВОЕМ ЗДОРОВЬЕ, ЗДОРОВЬЕ СВОИХ СОТРУДНИКОВ, КЛИЕНТОВ ИЛИ ПАРТНЁРОВ?! ТОГДА НАШЕ ПРЕДЛОЖЕНИЕ ОБЯЗАТЕЛЬНО ВАС ЗАИНТЕРЕСУЕТ, ПОТОМУ ЧТО МЫ ПРЕДЛАГАЕМ ПРОДУКТ, КОТОРЫЙ ОБЕСПЕЧИТ ВЫСОКИЙ УРОВЕНЬ ЗАЩИТЫ ЗДОРОВЬЯ!') !!}
            </p>
        </div>
    </div>

    <div class="container img-control section desktop">
        <div class="prop">
            <div class="prop_img prop_img-70">
                @if(str_replace("","",strip_tags(s_("Youtube Ссылка Antiseptic","antiseptic.Youtube","",""))) !="")
                    <a target="_blank" href="{!! s_("Youtube ссылка antiseptic","antiseptic.Youtube","","") !!}"
                       class="video_yy"
                       style="background-image: url('{!! s_("Youtube превью Antiseptic","antiseptic.Youtube","/public/media/client/images/open_video.png","") !!}');">
                    </a>
                @endif
                <div class="prop_img_src"
                     style="background-image:url('{!! s_("Фон первый блок antiseptic","antiseptic.Фоны","/public/media/client/images/man.png","images") !!}');background-size: cover;">
                    {{--                    <div class="text-container">--}}
                    {{--                        <div class="text-item">--}}
                    {{--                            <div class="text-item_head">--}}
                    {{--                                <p class="text text-s18">--}}
                    {{--                                    {!! strip_tags(s_("1 блок красный левый текст","antiseptic.Главная",'АНТИСЕПТИЧЕСКИЕ И ДЕЗИНФИЦИРУЮЩИЕ СРЕДСТВА "AVRORA HOLDING"')) !!}--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="text-item_main">--}}
                    {{--                                <p class="text text-s11">--}}
                    {{--                                    {!! strip_tags(s_("1 блок черный левый текст","antiseptic.Главная",'это эффективный способ оградить себя от инфекционных заболеваний и высокий уровень личной гигиены кожи в любых ситуациях!')) !!}--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}


                    {{--                        <div class="list">--}}


                    {{--                            @php--}}
                    {{--                                $desk=  explode(";",strip_tags(s_("1 блок список разедлитель ;","antiseptic.Главная","","textarea")));--}}
                    {{--                            @endphp--}}
                    {{--                            @foreach($desk as $ds)--}}
                    {{--                                @if(str_replace(" ","",$ds))--}}
                    {{--                                    <div class="list_it">--}}
                    {{--                                        <span class="text text-cal text-s11">{{$ds}}</span>--}}
                    {{--                                    </div>--}}
                    {{--                                @endif--}}
                    {{--                            @endforeach--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>


    <div class="container img-control section tablet">
        <div class="prop">
            <div class="prop_img">
                @if(str_replace("","",strip_tags(s_("Youtube Ссылка Antiseptic","antiseptic.Youtube","",""))) !="")
                    <a target="_blank" href="{!! s_("Youtube ссылка antiseptic","antiseptic.Youtube","","") !!}"
                       class="video_yy"
                       style="background-image: url('{!! s_("Youtube превью Antiseptic","antiseptic.Youtube","/public/media/client/images/open_video.png","") !!}');">
                    </a>
                @endif
                <img
                    src="{!! s_("Фон первый блок планшет antiseptic","antiseptic.Фоны","/public/media/client/images/man.png","images") !!}"
                    alt="">
            </div>
        </div>
    </div>

    <div class="container img-control section mobile">
        <div class="prop">
            <div class="prop_img">
                <img
                    src="{!! s_("Фон первый блок мобилка antiseptic","antiseptic.Фоны","/public/media/client/images/man.png","images") !!}"
                    alt="">
                @if(str_replace("","",strip_tags(s_("Youtube Ссылка Antiseptic","antiseptic.Youtube","",""))) !="")
                    <a target="_blank" href="{!! s_("Youtube ссылка antiseptic","antiseptic.Youtube","","") !!}"
                       class="video_mob">
                        <img
                            src="{!! s_("Youtube превью Antiseptic","antiseptic.Youtube","/public/media/client/images/open_video.png","") !!}"
                            alt="">
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="container  section new-block tablet">
        <div class="prop">
            <div class="prop_img">
                @if(str_replace("","",strip_tags(s_("Youtube Ссылка2 Antiseptic","antiseptic.Youtube","",""))) !="")
                    <a target="_blank" href="{!! s_("Youtube ссылка2 antiseptic","antiseptic.Youtube","","") !!}"
                       class="video_yy"
                       style="background-image: url('{!! s_("Youtube превью2 Antiseptic","antiseptic.Youtube","/public/media/client/images/open_video.png","") !!}');">
                    </a>
                @endif
                <img
                    src="{!! s_("Фон второй блок планшет antiseptic","antiseptic.Фоны","/public/media/client/images/man.png","images") !!}"
                    alt="">
            </div>
        </div>
    </div>

    <div class="container  section new-block mobile">
        <div class="prop">
            <div class="prop_img ">
                <img
                    src="{!! s_("Фон второй блок мобилка antiseptic","antiseptic.Фоны","/public/media/client/images/man.png","images") !!}"
                    alt="">

                @if(str_replace("","",strip_tags(s_("Youtube Ссылка2 Antiseptic","antiseptic.Youtube","",""))) !="")
                    <a target="_blank" href="{!! s_("Youtube ссылка2 antiseptic","antiseptic.Youtube","","") !!}"
                       class="video_mob">
                        <img
                            src="{!! s_("Youtube превью2 Antiseptic","antiseptic.Youtube","/public/media/client/images/open_video.png","") !!}"
                            alt="">
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="container  section new-block desktop">
        <div class="prop">
            <div class="prop_img prop_img-70">
                @if(str_replace("","",strip_tags(s_("Youtube Ссылка2 Antiseptic","antiseptic.Youtube","",""))) !="")
                    <a target="_blank" href="{!! s_("Youtube ссылка2 antiseptic","antiseptic.Youtube","","") !!}"
                       class="video_yy"
                       style="background-image: url('{!! s_("Youtube превью2 Antiseptic","antiseptic.Youtube","/public/media/client/images/open_video.png","") !!}');">
                    </a>
                @endif
                <div class="prop_img_src"
                     style="background-image:url('{!! s_("Фон второй блок antiseptic","antiseptic.Фоны","/public/media/client/images/man.png","images") !!}');background-size: cover;">
                    {{--                    <div class="text-container text-container-tho">--}}
                    {{--                        <div class="text-item">--}}
                    {{--                            <div class="text-item_head text-item_head-thos">--}}
                    {{--                                <p class="text text-s18">--}}
                    {{--                                    {!! strip_tags(s_("2 блок красный верхний текст","antiseptic.Главная",'ЛОКТЕВОГО ДОЗАТОР')) !!}--}}
                    {{--                                </p>--}}
                    {{--                                <p class="text text-s27">--}}
                    {{--                                    {!! strip_tags(s_("2 блок красный нижний текст","antiseptic.Главная",'"AVRORA PRO"')) !!}--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="text-item_main text-item_main-two">--}}
                    {{--                                <p class="text text-s11">--}}
                    {{--                                    {!! strip_tags(s_("2 блок описание под красным текстом","antiseptic.Главная",'Предназначен для эффективного и экономичного использования антисептических и дезинфицирующих средств для рук.')) !!}--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}


                    {{--                        <h2>--}}
                    {{--                            <span--}}
                    {{--                                class="text text-cal text-s18"><b> {!! strip_tags(s_("2 блок черный заголовок","antiseptic.Главная",'ПРЕИМУЩЕСТВА:')) !!}</b></span>--}}
                    {{--                        </h2>--}}
                    {{--                        <div class="list">--}}
                    {{--                            @php--}}
                    {{--                                $desk=   explode(";",strip_tags(s_("2 блок список разедлитель ;","antiseptic.Главная",'',"textarea")));--}}
                    {{--                            @endphp--}}
                    {{--                            @foreach($desk as $ds)--}}
                    {{--                                @if(str_replace(" ","",$ds))--}}
                    {{--                                    <div class="list_it">--}}
                    {{--                                        <span class="text text-cal text-s11">{{$ds}}</span>--}}
                    {{--                                    </div>--}}
                    {{--                                @endif--}}
                    {{--                            @endforeach--}}
                    {{--                        </div>--}}

                    {{--                        <div class="end_text">--}}
                    {{--                            <div class="text text-s11">--}}
                    {{--                                --}}{{--                                <b>ОБЕСПЕЧИВАЕТ</b> правильную технологию обработки рук персонала в медицинских,--}}
                    {{--                                --}}{{--                                лечебно-профилактических учреждениях, организациях общественного питания, школьных--}}
                    {{--                                --}}{{--                                учреждениях, учреждениях--}}
                    {{--                                --}}{{--                                соцобеспечения (дома престарелых, инвалидов и др.), маникюрных салонах, промышленных--}}
                    {{--                                --}}{{--                                предприятиях, офисах и гостиницах.--}}
                    {{--                                {!! (s_("2 блок описание под списком","antiseptic.Главная",'',"textarea")) !!}--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="container section table-block " >
        <div class="tt">
            <div class="ttt">
                <h2 class="hed-text ttt-text">
                    <b class="text text-s26"
                       style="color:#ED3237"> {!! strip_tags(s_("3 блок заголовок","antiseptic.Главная",'Ценовое предложение')) !!}</b>
                </h2>

                <table class="table">
                    <tbody>
                    @foreach(\App\Product::orderby("sort")->get() as $prs)
                        <tr>
                            <td>
                                <p class="text text-s26">
                                    <b>{{strip_tags(LC($prs->title))}}</b>
                                </p>
                            </td>
                            <td>
                                <p class="text text-s26">
                                    <b>{{LC($prs->price)}} тг</b>
                                </p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="footer-text">
                <p style="font-size:0.9rem;margin-bottom: 10px">{!! strip_tags(s_("4 блок заголовок","antiseptic.Главная",'Хотите узнать больше о нас?')) !!}</p>
                <div class="payments">
                    <img class="payments_img_src"
                         src="{!! s_("Способы Оплаты Изображение 1 Antiseptic","antiseptic.Главная","/public/media/client/disbarrier/images/kaspi1.png","images") !!}">
                    <img class="payments_img_src"
                         src="{!! s_("Способы Оплаты Изображение 2 Antiseptic","antiseptic.Главная","/public/media/client/disbarrier/images/kaspi2.png","images") !!}">
                </div>
                <div style="font-size:0.9rem;">
                    {!! (s_("4 блок описание","antiseptic.Главная",'',"textarea")) !!}
                </div>
                {{--            <div class="text text-cal text-s19">--}}
                {{--                {!! (s_("3 блок описание","antiseptic.Главная",'',"textarea")) !!}--}}
                {{--            </div>--}}
            </div>

        </div>
    </div>




    <div class="container  section section-end">
        <div class="footer">
            <div class="footer_main">
                <div class="text text-cal">
                    <b>{!! strip_tags(s_("5 Блок заголовок antiseptic","antiseptic.Главная",'Хотите узнать больше о нас?')) !!}</b>
                    <br>
                    {!! (s_("5 Блок контакты antiseptic","antiseptic.Главная",'',"textarea")) !!}
                </div>
                <div class="footer_buttons">
                    <a href="javascript:void()" style="position: static" class="btn btn-link btn-link-new open-model">
                    <span class="text text-cal text-s19"
                          style="color:#fff;text-transform: uppercase;">{!! s_("Кнопка обратная связь antiseptic","antiseptic.Главная","обратная связь") !!}</span>
                    </a>
                    <a href="{!! strip_tags(s_("4 блок кнопка ссылка","antiseptic.Главная",'')) !!}"
                       style="position:static"
                       target="_blank"
                       class="btn btn-link">
                    <span
                        class="text text-cal text-s19">{!! strip_tags(s_("4 блок кнопка текст","antiseptic.Главная",'ПЕРЕЙТИ НА САЙТ')) !!}</span>
                    </a>
                </div>
            </div>

        </div>

    </div>


    <div class="footer_after container">
        <div class="text textblas text-s10">
            {!! s_("Подвал текст antiseptic","antiseptic.Главная","ПЕРЕЙТИ НА САЙТ","textarea") !!}
        </div>
        <div class="social">

            <a href="{!! s_("Cсылка facebook antiseptic","antiseptic.Главная","ПЕРЕЙТИ НА САЙТ") !!}" class="soc">
                <img src="/public/media/client/disbarrier/images/faceboo.png" alt="">
            </a>

            <a href="{!! s_("Cсылка Instagram antiseptic","antiseptic.Главная","ПЕРЕЙТИ НА САЙТ") !!}" class="soc">
                <img src="/public/media/client/disbarrier/images/instag.png" alt="">
            </a>

            <a href="{!! s_("Cсылка youtube antiseptic","antiseptic.Главная","ПЕРЕЙТИ НА САЙТ") !!}" class="soc">
                <img src="/public/media/client/disbarrier/images/yout.png" alt="">
            </a>

        </div>
    </div>

    <style>
        strong {
            font-weight: bold;
        }
    </style>
@endsection
