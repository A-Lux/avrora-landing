import jquery from 'jquery';
import Masonry from 'masonry-layout'
import Swiper from 'swiper'


window.$ = window.jQuery = jquery;
require('@fancyapps/fancybox');
require('../../../../../node_modules/jquery-ui-bundle/jquery-ui');
import {init, mapinit} from '../../../mics/js/mapsMy.js';
import {jsend, getFormData} from '../../../mics/js/app';
//$.fancybox.open('htmlCode');

$(document).on("click", ".btn-prev", function (e) {
    window.history.back();
});
$(document).on("click", ".cli", function (e) {
    var
        $this = $(this),
        closest = $this.data("closest"),
        closestCli = $this.data("closestcli"),
        unDefain = $this.data("un"),
        target = $this.data("target"),
        $target = $this.data("target");

    if (typeof closest != "undefined") {
        $this = $($this.closest(closest));
    }


    if (typeof unDefain == "undefined") {
        unDefain = 0;
    } else {
        unDefain = 1;
    }

    if (typeof target != "undefined") {
        $target = $(target);
    }


    if (typeof closestCli != "undefined") {


        $(".cli", $this.closest(closestCli)).removeClass("active");
        $this.addClass("active");


    } else if (unDefain == 0) {

        if ($this.hasClass("active")) {
            $this.removeClass("active");
        } else {
            $this.addClass("active");
        }

    } else if (unDefain != 0) {


        $this.addClass("active").siblings().removeClass("active");


        if (typeof target != "undefined") {


            $target.eq($this.index()).addClass("active").siblings().removeClass("active");

        }

    }
});

$(document).on("click", ".open", function (e) {
    var
        $this = $(this);
    var
        $target = $($this.data("target"));
    $.fancybox.open($target.html());

});


window.initMaps = function (data) {
    var cors = (data["city"]["coordinates"]).split(",");

    if (cors.length == 2) {
        window.maspInit.setCenter("maps", cors);

        $(".conshops").html("");
        ymaps.ready(function () {
            $.each(data["shop"], function (key, shop) {

                var shopSing = (shop["coordinates"].split(","));
                if (shopSing.length == 2) {
                    window.maspInit.addPointCustom("maps", {
                        "coordinates": shopSing,
                        "hintContent": shop["name"]
                    }, "/public/media/client/images/placeholder-filled-point.png");
                }

                $(".conshops").append('<a href="javascript:void(0)" class="li_maps openShop" data-id="' + shop["id"] + '"> <p> <img src="/public/media/client/images/placeholder-filled-point.png" alt=""> <span class="text text-s16"><b>' + shop["name"] + '</b></span> </p> </a>');

            });
            if (data["shop"].length == 0) {
                $(".conshops").append('<div class="li_maps" style="padding-left: 1rem;"> <p>  <span class="text text-s16"><b>Товар в этом городе не найден</b></span> </p> </div>');
            }
        });
        //
    }

}


window.shopSet = function (data) {

    if (data != "false") {

        $(".shopGet").html(data);
        $(".product_maps_nav").addClass("active");

    }


}

$(function () {


    if ($("*").is("#city_select")) {
        ymaps.ready(function () {
            window.maspInit = new mapinit();
            window.maspInit.init("maps");

            if ($("*").is("#maps_con")) {
                window.maspInit.init("maps_con");
            }

            jsend($("#city_select").data("url"), {
                "id": $("#city_select").val(),
                "product_id": $("#city_select").data("product")
            }, "window.initMaps(data);");
        });
    }


    $(document).on("click", ".open-model", function () {
        $.fancybox.open($(".form").html());
    });

    $(document).on("click", ".prevBack", function () {
        $(".product_maps_nav").removeClass("active");
        $(".shopGet").html('');
    });

    $(document).on("click", ".menu_open", function () {
        $("body").addClass("body_menu_open");
    });

    $(document).on("click", ".close", function () {
        $("body").removeClass("body_menu_open");
    });


    $(document).on("click", ".openShop", function () {
        jsend($(".product_maps_nav").data("url"), {
            "id": $(this).data("id")
        }, "window.shopSet(data);");
    });

    $(document).on("change", ".selcity", function () {
        jsend($(this).data("url"), {
            "id": $(this).val(),
            "product_id": $("#city_select").data("product")
        }, "window.initMaps(data);");
    });


    window.sas = function (data) {


    }

    $(document).on("submit", ".sen_fors", function () {
        var $this = $(this);
        var inputs = getFormData($this);
        jsend($this.prop("action"), inputs, "window.sas(data);", "");
        $.fancybox.close();
        return false;
    });


    $(".slider_ui").each(function () {


        var $this = $(this);
        var min = $this.data("min");
        var max = $this.data("max");
        var step = $this.data("step");

        $this.slider({
            min: min,
            max: max,
            step: step,
            create: function () {
                $(".custom-handle", $this).text($(this).slider("value"));
                $(".select_input", $this.parent()).val($(this).slider("value"));
            },
            slide: function (event, ui) {
                $(".custom-handle", $this).text(ui.value);
                $(".select_input", $this.parent()).val(ui.value);

            },
            stop: function (event, ui) {
                $this.closest("form").submit();
            }
        });

    });

    $(document).on("change", ".cheketitem input", function () {

        $(this).closest("form").submit();
    });

    $(".filters").submit();

    $('.swiper-container').each(function () {
        var
            $this = $(this),
            lg = $this.data("lg"),
            nb = $this.data("nb"),
            md = $this.data("md"),
            sm = $this.data("sm"),
            xs = $this.data("xs"),
            bw = $this.data("bw"),
            slidesPerColumn = $this.data("col"),
            pagination = $this.data("pagi"),
            directionData = $this.data("direction"),
            next = $this.data("next"),
            $nav = {};

        if (typeof next == "undefined") {
            next = false;
        }
        if (typeof directionData == "undefined") {
            directionData = "horizontal";

        }

        if (typeof pagination == "undefined") {
            pagination = "";
        }


        if (typeof lg == "undefined") {
            lg = 1;
        }

        if (typeof nb == "undefined") {
            nb = lg;
        }

        if (typeof md == "undefined") {
            md = nb;
        }

        if (typeof sm == "undefined") {
            sm = md;
        }

        if (typeof xs == "undefined") {
            xs = sm;
        }

        if (typeof bw == "undefined") {
            bw = 0;
        }
        if (typeof slidesPerColumn == "undefined") {
            slidesPerColumn = 1;
        }

        if (next == true) {
            $nav = {
                nextEl: $('.swiper-button-next', $(this).parent()),
                prevEl: $('.swiper-button-prev', $(this).parent()),
            };
        }

        var swiper = new Swiper(this, {
            slidesPerView: lg,
            spaceBetween: bw,
            slidesPerColumn: slidesPerColumn,
            direction: directionData,
            loop: false,
            lazy: true,
            navigation: $nav,
            slideToClickedSlide: true,


            pagination: {
                el: $('.swiper-pagination', $(this).parent()),
            },

            breakpoints: {
                2400: {
                    slidesPerView: lg,
                },
                1375: {
                    slidesPerView: nb,
                },
                1030: {
                    slidesPerView: md,
                },
                780: {
                    slidesPerView: sm,
                },
                640: {
                    slidesPerView: xs,
                }
            }
        });

        $this.addClass("active");
        if (pagination != "") {
            $(".swiper-pagination-bullet", $this).click(function () {
                swiper.slideTo($(this).index());
                $(this).addClass('active').siblings().removeClass("active");
            });
        }
    });


});



